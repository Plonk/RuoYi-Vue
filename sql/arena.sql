create table if not exists arena_info
(
    id          bigint unsigned auto_increment comment '主键id' primary key,
    dept_id     bigint not null comment '部门id',
    show_name   varchar(64) comment '对外展示名称',
    contact     varchar(32) comment '联系人',
    mobile      varchar(11) comment '球馆联系电话',
    address     varchar(128) comment '球馆地址',
    remark      varchar(256) comment '备注',
    create_time datetime default now(),
    creator     bigint comment '创建人id'
) charset = utf8mb4 comment '球馆详情表'
  collate = utf8mb4_unicode_ci;

create table if not exists arena_member
(
    id            bigint unsigned auto_increment comment '主键id' primary key,
    nickname      varchar(64) comment '昵称',
    mobile        varchar(11) comment '电话',
    wechat_uni_id varchar(128) comment '微信全局id',
    remark        varchar(256) comment '备注',
    create_time   datetime default now(),
    creator       bigint comment '创建人id',
    unique unq_mobile (mobile)
) charset = utf8mb4 comment '球馆会员表'
  collate = utf8mb4_unicode_ci;

create table if not exists arena_member_relation
(
    id          bigint unsigned auto_increment comment '主键id' primary key,
    mobile      varchar(11) comment '会员电话',
    arena_id    bigint comment '球馆id',
    score       int comment '积分',
    score_rank  int comment '积分排名',
    create_time datetime default now() comment '关联时间',
    index idx_mobile (mobile),
    index idx_arena (arena_id)
) charset = utf8mb4 comment '球馆会员中间表'
  collate = utf8mb4_unicode_ci;

create table if not exists race_info
(
    id             bigint unsigned auto_increment comment '主键id' primary key,
    arena_id       bigint not null comment '举办球馆id',
    single         tinyint comment '0 多家球馆参赛 1 本球馆参赛',
    title          varchar(64) comment '比赛标题',
    max_join       int      default 0 comment '最大参与人数',
    apply_end_time datetime comment '报名截止时间',
    begin_time     datetime comment '比赛开始时间',
    first_score    int comment '冠军积分',
    second_score   int comment '亚军积分',
    third_score    int comment '季军积分',
    join_score     int comment '参与积分',
    create_time    datetime default now(),
    status         tinyint  default 1 comment '1未开始 2进行中 3已结束',
    creator        bigint comment '创建人id'
) charset = utf8mb4 comment '比赛信息表'
  collate = utf8mb4_unicode_ci;

create table if not exists race_arena
(
    id       bigint unsigned auto_increment comment '主键id' primary key,
    race_id  bigint not null comment '比赛id',
    arena_id bigint not null comment '参赛球馆id',
    home     tinyint comment '0 客场 1 主场'
) charset = utf8mb4 comment '参赛球馆表'
  collate = utf8mb4_unicode_ci;

create table if not exists race_member
(
    id                 bigint unsigned auto_increment comment '主键id' primary key,
    race_id            bigint not null comment '比赛id',
    arena_id           bigint not null comment '参赛球馆id',
    member_id          bigint not null comment '参赛会员id',
    race_member_number tinyint comment '运动员号码牌,按报名顺序生成',
    home               tinyint comment '0 客场 1 主场',
    unique uqi_race_member (race_id, member_id, race_member_number)
) charset = utf8mb4 comment '参赛会员表'
  collate = utf8mb4_unicode_ci;