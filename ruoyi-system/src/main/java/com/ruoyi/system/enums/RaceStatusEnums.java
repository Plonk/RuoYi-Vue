package com.ruoyi.system.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum RaceStatusEnums {
    /*** 比赛状态枚举  ***/
    UN_START(1, "未开始"),
    START(2, "进行中"),
    END(3, "已结束"),
    ;
    @EnumValue
    private final int code;
    private final String name;

    RaceStatusEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
