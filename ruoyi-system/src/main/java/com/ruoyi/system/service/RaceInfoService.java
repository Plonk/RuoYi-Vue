package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.RaceInfo;

/**
 * 比赛信息表(RaceInfo)表服务接口
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
public interface RaceInfoService extends IService<RaceInfo> {

}
