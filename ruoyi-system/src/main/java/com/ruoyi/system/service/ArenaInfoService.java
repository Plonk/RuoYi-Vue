package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.ArenaInfo;

import java.util.List;

/**
 * 球馆详情表(ArenaInfo)表服务接口
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:39
 */
public interface ArenaInfoService extends IService<ArenaInfo> {

    List<ArenaInfo> listArena(ArenaInfo query);
}
