package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.RaceMember;
import com.ruoyi.system.mapper.RaceMemberMapper;
import com.ruoyi.system.service.RaceMemberService;
import org.springframework.stereotype.Service;

/**
 * 参赛会员表(RaceMember)表服务实现类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
@Service("raceMemberService")
public class RaceMemberServiceImpl extends ServiceImpl<RaceMemberMapper, RaceMember> implements RaceMemberService {

}
