package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.ArenaMemberRelation;
import com.ruoyi.system.mapper.ArenaMemberRelationMapper;
import com.ruoyi.system.service.ArenaMemberRelationService;
import org.springframework.stereotype.Service;

/**
 * 球馆会员中间表(ArenaMemberRelation)表服务实现类
 *
 * @author mbp-tmp
 * @since 2021-09-26 21:27:39
 */
@Service("arenaMemberRelationService")
public class ArenaMemberRelationServiceImpl extends ServiceImpl<ArenaMemberRelationMapper, ArenaMemberRelation> implements ArenaMemberRelationService {

}
