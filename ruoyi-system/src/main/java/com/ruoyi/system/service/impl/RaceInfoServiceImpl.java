package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.RaceInfo;
import com.ruoyi.system.mapper.RaceInfoMapper;
import com.ruoyi.system.service.RaceInfoService;
import org.springframework.stereotype.Service;

/**
 * 比赛信息表(RaceInfo)表服务实现类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
@Service("raceInfoService")
public class RaceInfoServiceImpl extends ServiceImpl<RaceInfoMapper, RaceInfo> implements RaceInfoService {

}
