package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.ArenaMemberRelation;

/**
 * 球馆会员中间表(ArenaMemberRelation)表服务接口
 *
 * @author mbp-tmp
 * @since 2021-09-26 21:27:39
 */
public interface ArenaMemberRelationService extends IService<ArenaMemberRelation> {

}
