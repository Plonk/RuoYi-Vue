package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.ArenaInfo;
import com.ruoyi.system.mapper.ArenaInfoMapper;
import com.ruoyi.system.service.ArenaInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 球馆详情表(ArenaInfo)表服务实现类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:39
 */
@Service("arenaInfoService")
public class ArenaInfoServiceImpl extends ServiceImpl<ArenaInfoMapper, ArenaInfo> implements ArenaInfoService {

    @Override
    public List<ArenaInfo> listArena(ArenaInfo query) {
        return baseMapper.listCustom(query);
    }
}
