package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.RaceArena;
import com.ruoyi.system.mapper.RaceArenaMapper;
import com.ruoyi.system.service.RaceArenaService;
import org.springframework.stereotype.Service;

/**
 * 参赛球馆表(RaceArena)表服务实现类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
@Service("raceArenaService")
public class RaceArenaServiceImpl extends ServiceImpl<RaceArenaMapper, RaceArena> implements RaceArenaService {

}
