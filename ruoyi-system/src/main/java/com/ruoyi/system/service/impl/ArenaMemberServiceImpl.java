package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.ArenaMember;
import com.ruoyi.system.domain.ArenaMemberRelation;
import com.ruoyi.system.dto.MemberListDTO;
import com.ruoyi.system.dto.MemberQueryDTO;
import com.ruoyi.system.mapper.ArenaMemberMapper;
import com.ruoyi.system.service.ArenaMemberRelationService;
import com.ruoyi.system.service.ArenaMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * 球馆会员表(ArenaMember)表服务实现类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
@Service("arenaMemberService")
public class ArenaMemberServiceImpl extends ServiceImpl<ArenaMemberMapper, ArenaMember> implements ArenaMemberService {
    @Autowired
    private ArenaMemberRelationService relationService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveAndRelation(ArenaMember member, List<Long> arenaId) {
        save(member);

        /*
        将会员和球馆进行关联
        避免同一球馆会员重复
         */
        Long id = member.getId();
        relation(id, arenaId);
    }

    /**
     * 将会员和球馆进行关联
     * 避免同一球馆会员重复
     */
    @Override
    public void relation(Long memberId, List<Long> arenaId) {
        if (Objects.isNull(memberId) || CollectionUtils.isEmpty(arenaId)) {
            return;
        }
        arenaId.forEach(id -> {
            ArenaMemberRelation relation = new ArenaMemberRelation();
            relation.setMemberId(memberId);
            relation.setArenaId(id);
            relationService.save(relation);
        });
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateMember(ArenaMember member) {
        Long id = member.getId();
        ArenaMember db = getById(id);
        if (Objects.isNull(db)) {
            return;
        }
        updateById(member);
    }

    @Override
    public List<MemberListDTO> pageList(MemberQueryDTO dto) {
        return baseMapper.pageList(dto);
    }
}
