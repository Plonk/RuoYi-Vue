package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.ArenaMember;
import com.ruoyi.system.dto.MemberListDTO;
import com.ruoyi.system.dto.MemberQueryDTO;

import java.util.List;

/**
 * 球馆会员表(ArenaMember)表服务接口
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:39
 */
public interface ArenaMemberService extends IService<ArenaMember> {

    /**
     * 保存会员并关联球馆
     *
     * @param member
     * @param arenaId
     */
    void saveAndRelation(ArenaMember member, List<Long> arenaId);

    /**
     * 关联会员
     *
     * @param memberId
     * @param arenaId
     */
    void relation(Long memberId, List<Long> arenaId);

    /**
     * 修改会员
     *
     * @param member
     */
    void updateMember(ArenaMember member);

    List<MemberListDTO> pageList(MemberQueryDTO dto);
}
