package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.RaceArena;

/**
 * 参赛球馆表(RaceArena)表服务接口
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
public interface RaceArenaService extends IService<RaceArena> {

}
