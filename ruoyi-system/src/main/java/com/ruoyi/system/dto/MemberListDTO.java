package com.ruoyi.system.dto;

import lombok.Data;

/**
 * @author yanhonghao Created on 2021/9/27.
 */
@Data
public class MemberListDTO {
    private String mobile;
    private Long id;
    private String remark;
    private Long arenaId;
    private Integer score;
    private Integer scoreRank;
}
