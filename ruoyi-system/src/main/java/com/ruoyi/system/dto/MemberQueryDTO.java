package com.ruoyi.system.dto;

import lombok.Data;

/**
 * @author yanhonghao Created on 2021/9/27.
 */
@Data
public class MemberQueryDTO {
    private String mobile;
    private Long arenaId;
    private String nickname;
}
