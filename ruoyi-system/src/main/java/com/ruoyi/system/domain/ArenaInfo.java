package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 球馆详情表(ArenaInfo)表实体类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ArenaInfo extends Model<ArenaInfo> {
    /*** 主键id **/
    @TableId(type = IdType.AUTO)
    private Long id;
    /*** 部门id **/
    private Long deptId;
    /*** 对外展示名称 **/
    private String showName;
    /*** 联系人 **/
    private String contact;
    /*** 球馆联系电话 **/
    private String mobile;
    /*** 球馆地址 **/
    private String address;
    /*** 备注 **/
    private String remark;
    private LocalDateTime createTime;
    /*** 创建人id **/
    private Long creator;
    /*** 逻辑删除 **/
    private Integer deleted;
    @TableField(exist = false)
    private String deptName;
}
