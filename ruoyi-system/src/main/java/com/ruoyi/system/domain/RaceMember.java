package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 参赛会员表(RaceMember)表实体类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RaceMember extends Model<RaceMember> {
    /*** 主键id **/
    @TableId(type = IdType.AUTO)
    private Long id;
    /*** 比赛id **/
    private Long raceId;
    /*** 参赛球馆id **/
    private Long arenaId;
    /*** 参赛会员id **/
    private Long memberId;
    /*** 运动员号码牌,按报名顺序生成 **/
    private Integer raceMemberNumber;
    /*** 0 客场 1 主场 **/
    private Integer home;
}
