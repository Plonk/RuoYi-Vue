package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 球馆会员中间表(ArenaMemberRelation)表实体类
 *
 * @author mbp-tmp
 * @since 2021-09-26 21:27:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ArenaMemberRelation extends Model<ArenaMemberRelation> {
    /*** 主键id **/
    @TableId(type = IdType.AUTO)
    private Long id;
    /*** 会员id **/
    private Long memberId;
    /*** 球馆id **/
    private Long arenaId;
    /*** 关联时间 **/
    private LocalDateTime createTime;
    /*** 积分 **/
    private Integer score;
    /*** 积分排名 **/
    private Integer scoreRank;
}
