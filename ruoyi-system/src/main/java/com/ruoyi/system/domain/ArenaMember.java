package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 球馆会员表(ArenaMember)表实体类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ArenaMember extends Model<ArenaMember> {
    /*** 主键id **/
    @TableId(type = IdType.AUTO)
    private Long id;
    /*** 昵称 **/
    private String nickname;
    /*** 电话 **/
    private String mobile;
    /*** 微信全局id **/
    private String wechatUniId;
    /*** 备注 **/
    private String remark;
    private LocalDateTime createTime;
    /*** 创建人id **/
    private Long creator;
    /*** 逻辑删除 **/
    private Integer deleted;
}
