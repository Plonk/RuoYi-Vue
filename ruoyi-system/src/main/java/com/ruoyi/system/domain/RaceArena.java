package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 参赛球馆表(RaceArena)表实体类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RaceArena extends Model<RaceArena> {
    /*** 主键id **/
    @TableId(type = IdType.AUTO)
    private Long id;
    /*** 比赛id **/
    private Long raceId;
    /*** 参赛球馆id **/
    private Long arenaId;
    /*** 0 客场 1 主场 **/
    private Integer home;
}
