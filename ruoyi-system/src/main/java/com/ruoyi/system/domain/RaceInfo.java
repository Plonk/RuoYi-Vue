package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.ruoyi.system.enums.RaceStatusEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 比赛信息表(RaceInfo)表实体类
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RaceInfo extends Model<RaceInfo> {
    /*** 主键id **/
    @TableId(type = IdType.AUTO)
    private Long id;
    /*** 举办球馆id **/
    private Long arenaId;
    /*** 0 多家球馆参赛 1 本球馆参赛 **/
    private Integer single;
    /*** 比赛标题 **/
    private String title;
    /*** 最大参与人数 **/
    private Integer maxJoin;
    /*** 报名截止时间 **/
    private LocalDateTime applyEndTime;
    /*** 比赛开始时间 **/
    private LocalDateTime beginTime;
    /*** 冠军积分 **/
    private Integer firstScore;
    /*** 亚军积分 **/
    private Integer secondScore;
    /*** 季军积分 **/
    private Integer thirdScore;
    /*** 参与积分 **/
    private Integer joinScore;
    private LocalDateTime createTime;
    /*** 创建人id **/
    private Long creator;
    private RaceStatusEnums status;
}
