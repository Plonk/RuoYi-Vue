package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.ArenaMemberRelation;

/**
 * 球馆会员中间表(ArenaMemberRelation)表数据库访问层
 *
 * @author mbp-tmp
 * @since 2021-09-26 21:27:39
 */
public interface ArenaMemberRelationMapper extends BaseMapper<ArenaMemberRelation> {

}
