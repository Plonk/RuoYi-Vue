package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.RaceArena;

/**
 * 参赛球馆表(RaceArena)表数据库访问层
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
public interface RaceArenaMapper extends BaseMapper<RaceArena> {

}
