package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.ArenaInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 球馆详情表(ArenaInfo)表数据库访问层
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:39
 */
@Repository
public interface ArenaInfoMapper extends BaseMapper<ArenaInfo> {

    List<ArenaInfo> listCustom(@Param("search") ArenaInfo query);
}
