package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.RaceInfo;

/**
 * 比赛信息表(RaceInfo)表数据库访问层
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
public interface RaceInfoMapper extends BaseMapper<RaceInfo> {

}
