package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.ArenaMember;
import com.ruoyi.system.dto.MemberListDTO;
import com.ruoyi.system.dto.MemberQueryDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 球馆会员表(ArenaMember)表数据库访问层
 *
 * @author mbp-tmp
 * @since 2021-09-16 22:52:40
 */
public interface ArenaMemberMapper extends BaseMapper<ArenaMember> {

    List<MemberListDTO> pageList(@Param("search") MemberQueryDTO dto);
}
