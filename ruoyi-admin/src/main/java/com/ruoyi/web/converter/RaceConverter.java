package com.ruoyi.web.converter;

import com.ruoyi.system.domain.RaceInfo;
import com.ruoyi.web.controller.tool.CopyUtils;
import com.ruoyi.web.model.RacePostVO;
import org.springframework.stereotype.Component;

@Component
public class RaceConverter {

    public RaceInfo convert(RacePostVO vo) {
        RaceInfo info = new RaceInfo();
        CopyUtils.copy(vo, info);
        return info;
    }
}
