package com.ruoyi.web.converter;

import com.ruoyi.system.domain.ArenaInfo;
import com.ruoyi.web.controller.tool.CopyUtils;
import com.ruoyi.web.model.ArenaListVO;
import org.springframework.stereotype.Component;

@Component
public class ArenaConverter {
    public ArenaListVO convertList(ArenaInfo info) {
        ArenaListVO listVO = new ArenaListVO();
        CopyUtils.copy(info, listVO);
        return listVO;
    }
}
