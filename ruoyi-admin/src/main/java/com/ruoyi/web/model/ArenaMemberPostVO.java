package com.ruoyi.web.model;

import lombok.Data;

import java.util.List;

@Data
public class ArenaMemberPostVO {
    private Long memberId;
    /*** 球馆id **/
    private List<Long> arenaId;
    /*** 昵称 **/
    private String nickname;
    /*** 电话 **/
    private String mobile;
    /*** 微信全局id **/
    private String wechatUniId;
    /*** 备注 **/
    private String remark;
}
