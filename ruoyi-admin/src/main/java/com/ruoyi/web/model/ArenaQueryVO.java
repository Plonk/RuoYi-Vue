package com.ruoyi.web.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ArenaQueryVO {
    private Long id;
    private Long deptId;
    private String showName;
    private String contact;
    private String mobile;
    private String remark;
    private LocalDateTime createTime;
    private String address;
}
