package com.ruoyi.web.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ArenaListVO {
    private Long id;
    private Long deptId;
    private String showName;
    private String contact;
    private String mobile;
    private String address;
    private String remark;
    private String deptName;
    private LocalDateTime createTime;
}
