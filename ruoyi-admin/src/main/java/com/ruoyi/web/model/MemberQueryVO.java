package com.ruoyi.web.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author yanhonghao Created on 2021/9/27.
 */
@Data
@ApiModel("会员查询对象")
public class MemberQueryVO {
    private String mobile;
    private Long arenaId;
    private String nickname;
}
