package com.ruoyi.web.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("保存、修改比赛页面对象")
public class RacePostVO {
    private Long id;
    @ApiModelProperty("1球馆内部比赛 0联赛")
    private Integer single;
    @ApiModelProperty("比赛标题")
    private String title;
    @ApiModelProperty("人数限制")
    private Integer maxJoin;
    @ApiModelProperty("报名截止时间")
    private LocalDateTime applyEndTime;
    @ApiModelProperty("比赛开始时间")
    private LocalDateTime beginTime;
    @ApiModelProperty("冠军积分")
    private Integer firstScore;
    @ApiModelProperty("亚军积分")
    private Integer secondScore;
    @ApiModelProperty("季军积分")
    private Integer thirdScore;
    @ApiModelProperty("参赛可获得积分")
    private Integer joinScore;
}
