package com.ruoyi.web.controller.arena;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.ArenaInfo;
import com.ruoyi.system.service.ArenaInfoService;
import com.ruoyi.web.converter.ArenaConverter;
import com.ruoyi.web.utils.ConvertUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/arena")
public class ArenaController extends BaseController {
    @Autowired
    private ArenaInfoService arenaInfoService;
    @Autowired
    private ArenaConverter arenaConverter;

    @PostMapping("page")
    @ApiOperation("球馆列表")
    public TableDataInfo page(@RequestBody ArenaInfo query) {
        Long loginDeptId = SecurityUtils.getDeptId();
        //总租户可以查看其他部门球馆
        if (loginDeptId != 100) {
            query.setDeptId(loginDeptId);
        }
        startPage();
        List<ArenaInfo> list = arenaInfoService.listArena(query);
        return getDataTable(ConvertUtils.convertList(list, arenaConverter::convertList));
    }

    @PostMapping("add")
    @ApiOperation("新增球馆")
    @Log(title = "新增球馆", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody ArenaInfo query) {
        arenaInfoService.save(query);
        return AjaxResult.success();
    }

    @PostMapping("modify")
    @ApiOperation("修改球馆")
    @Log(title = "修改球馆", businessType = BusinessType.UPDATE)
    public AjaxResult modify(@RequestBody ArenaInfo query) {
        query.setDeptId(null);
        arenaInfoService.updateById(query);
        return AjaxResult.success();
    }

    @DeleteMapping("{id}")
    @ApiOperation("删除")
    @Log(title = "删除球馆", businessType = BusinessType.DELETE)
    public AjaxResult deleteById(@PathVariable Long id) {
        arenaInfoService.removeById(id);
        return AjaxResult.success();
    }
}
