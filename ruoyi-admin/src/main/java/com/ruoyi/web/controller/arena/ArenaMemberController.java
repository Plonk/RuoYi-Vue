package com.ruoyi.web.controller.arena;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ArenaMember;
import com.ruoyi.system.dto.MemberListDTO;
import com.ruoyi.system.dto.MemberQueryDTO;
import com.ruoyi.system.service.ArenaMemberService;
import com.ruoyi.web.controller.tool.CopyUtils;
import com.ruoyi.web.model.ArenaMemberPostVO;
import com.ruoyi.web.model.MemberQueryVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/arena/member")
public class ArenaMemberController extends BaseController {
    @Autowired
    private ArenaMemberService arenaMemberService;

    @PostMapping("page")
    @ApiOperation("球馆会员列表")
    public TableDataInfo page(MemberQueryVO vo) {
        MemberQueryDTO dto = new MemberQueryDTO();
        CopyUtils.copy(vo, dto);
        startPage();
        List<MemberListDTO> result = arenaMemberService.pageList(dto);
        return getDataTable(result);
    }

    @PostMapping
    @Log(title = "创建球馆会员", businessType = BusinessType.INSERT)
    public AjaxResult create(ArenaMemberPostVO vo) {
        String mobile = vo.getMobile();
        ArenaMember member = arenaMemberService.getOne(Wrappers.<ArenaMember>lambdaQuery()
                .eq(ArenaMember::getMobile, mobile).last("limit 1"));
        //若会员表中无数据 新增该会员 并且进行球馆关联
        List<Long> arenaId = vo.getArenaId();
        if (Objects.isNull(member)) {
            member = new ArenaMember();
            CopyUtils.copy(vo, member);
            arenaMemberService.saveAndRelation(member, arenaId);
        } else {
            //会员已存在 直接关联球馆
            arenaMemberService.relation(member.getId(), arenaId);
        }
        return AjaxResult.success();
    }

    @PutMapping
    @Log(title = "修改球馆会员", businessType = BusinessType.UPDATE)
    public AjaxResult update(ArenaMemberPostVO vo) {
        ArenaMember member = new ArenaMember();
        member.setNickname(vo.getNickname());
        member.setMobile(vo.getMobile());
        arenaMemberService.updateMember(member);
        return AjaxResult.success();
    }

    @PostMapping("relation")
    @Log(title = "关联球馆会员", businessType = BusinessType.UPDATE)
    public AjaxResult relation(ArenaMemberPostVO vo) {
        arenaMemberService.relation(vo.getMemberId(), vo.getArenaId());
        return AjaxResult.success();
    }
}
