package com.ruoyi.web.controller.tool;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.beans.BeanCopier;

@Slf4j
public class CopyUtils {

    public static void copy(Object source, Object target) {
        try {
            BeanCopier.create(source.getClass(), target.getClass(), false)
                    .copy(source, target, null);
        } catch (Exception e) {
            log.error("对象拷贝异常:" + e.getLocalizedMessage());
        }
    }
}
