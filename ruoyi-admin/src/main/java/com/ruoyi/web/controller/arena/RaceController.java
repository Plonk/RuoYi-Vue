package com.ruoyi.web.controller.arena;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.RaceInfo;
import com.ruoyi.system.enums.RaceStatusEnums;
import com.ruoyi.system.service.RaceInfoService;
import com.ruoyi.web.converter.RaceConverter;
import com.ruoyi.web.model.RacePostVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/arena")
public class RaceController {
    @Autowired
    private RaceInfoService raceInfoService;
    @Autowired
    private RaceConverter raceConverter;

    @PostMapping
    @Log(title = "创建比赛", businessType = BusinessType.INSERT)
    public AjaxResult create(@RequestBody RacePostVO vo) {
        RaceInfo info = raceConverter.convert(vo);
        info.setStatus(RaceStatusEnums.UN_START);
        raceInfoService.save(info);
        return AjaxResult.success();
    }

    @PutMapping
    @Log(title = "修改比赛", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody RacePostVO vo) {
        RaceInfo info = raceConverter.convert(vo);
        raceInfoService.saveOrUpdate(info);
        return AjaxResult.success();
    }

    @DeleteMapping
    @Log(title = "删除比赛", businessType = BusinessType.DELETE)
    public AjaxResult delete(@RequestParam Long id) {
        raceInfoService.removeById(id);
        return AjaxResult.success();
    }
}
