import request from '@/utils/request'

// 球馆列表
export function list(data) {
  return request({
    url: '/arena/page',
    method: 'post',
    data: data
  })
}

// 球馆新增
export function add(data) {
  return request({
    url: '/arena/add',
    method: 'post',
    data: data
  })
}

// 球馆修改
export function modify(data) {
  return request({
    url: '/arena/modify',
    method: 'post',
    data: data
  })
}

// 球馆删除
export function deleteById(id) {
  return request({
    url: `/arena/${id}`,
    method: 'delete'
  })
}
